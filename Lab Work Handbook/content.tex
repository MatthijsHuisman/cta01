% !TeX spellcheck = en_US
\hyphenation{Work-space-Window Work-space}
\begin{clstShortInline}
\chapter{Introduction}
\label{sec:introduction}
To fully understand how a computer program runs on computer hardware it is required to have knowledge not only about the computer hardware, but also on how to write machine language instructions that will be executed on the hardware.

Because we explain the concepts of computer engineering by looking at the LEGv8 processor \cite{patterson2016} (which is especially suitable for this purpose) we will also write assembly programs for this processor.
We will verify the programs by using a simulator, so we can easily single step instructions and look at the registers and memory in the meanwhile.
In this way we will get a better understanding of and insights in:
\begin{itemize}
	\item What type of instructions a computer will need to support to use to gain some desired functionality.
	\item How a compiler might translate high-level code into assembly.
	\item How to use memory (and especially the stack).
	\item How making smarter algorithms in the first place can speed up things later on.
\end{itemize}

During the lab we will only use the LEGv8 Core Instruction Set. These instructions are defined in your book \cite[Figure~2.1]{patterson2016} and can also be found in the first column
of the green LEGv8 reference data card which came with your book. You are not allowed to use the Arithmetic Core Instruction Set but you may use the four instructions listed in the Pseudoinstruction Set.  

You need to assemble (pun intended) a small report which contains all the assembler functions you have written, the C programs which you have used to test your assembler functions, and the outcomes of those tests. Also, the answers to the questions raised in the assignments should be included in the report. This report should be uploaded to the \enquote{inlevermap} before the deadline which is defined in the \enquote{modulewijzer}.

Good luck!

\chapter{Using ARM DS-5 and writing programs}
In this lab you will use the free Community Edition of ARM's Integrated Development Environment called DS-5.
Which can be downloaded from ARM\footnote{\url{https://developer.arm.com/products/software-development-tools/ds-5-development-studio/editions/ds-5-community-edition}} or from the server of Hogeschool Rotterdam\footnote{\code{//venus/STUD02/Opleiding/Elektrotechniek/2016-2017/CTA01/software}}. The installation does not need any further explanation.

You will also have to download a proper toolchain which includes an ARMv8 simulator. 
This toolchain can be downloaded from the Linaro website\footnote{\url{https://releases.linaro.org/components/toolchain/binaries/latest/aarch64-elf/gcc-linaro-6.1.1-2016.08-i686-mingw32_aarch64-elf.tar.xz}}.
Download this file and extract it to \code{C:\\}.

\section{Installing Linaro aarch64-elf GCC as a toolchain in DS-5}
You can add the toolchain in DS-5 as follows:
\begin{itemize}
	\item In DS-5, select Window → Preferences → DS-5 → Toolchains.
	\item In the Toolchains dialog, click Add.
	\item In the Select Toolchain Path dialog, enter the path to the toolchain binaries. This will normally be \hcode{C:\\gcc-linaro-6.1.1-2016.08-i686-mingw32_\\-aar\\-ch64-elf\\bin}. 
	Then click Next.
	\item In the Discovered Toolchain Information dialog, check the toolchain information is correct (as shown in \cref{fig:toolchain}), then click Finish.
	\item The toolchain appears in the Toolchains dialog, click Apply, then click Restart Eclipse.
\end{itemize}

% figuren staan in directory ./figs
% #1 OPTIONEEL = placement argumenten voor figure b.v. tb of H 
% #2 = width of scale optie voor \includegraphics
% #3 = filenaam (zonder extensie); labelnaam=fig:filenaam
% #4 = titel
\figuur{scale=.75}{toolchain}{The Discovered Toolchain Information dialog.}

\section{Running a C program on the ARM-v8 simulator}
Unzip the file |Bare-metal_examples_ARMv8.zip| which can be found in \hcode{C:\\\\-Pro\\-gram Files\\DS-5 CE v5.26.0\\examples\\}. 
For example into the directory \hcode{C:\\\\-Ba\\-re-metal_ex\\-amples_ARMv8\\}.

You can run the |calendar_ARMv8_GCC| example in DS-5 as follows:
\begin{itemize}
	\item In DS-5, select File → Import... →  General → Existing Projects into Workspace.
	Then click Next.
	\item Select the directory \hcode{C:\\\\-Ba\\-re-metal_ex\\-amples_ARMv8\\}.
	\item Check the |calendar_ARMv8_GCC| example and the Copy Projects into Workspace option, see \cref{fig:import}.
	Then click Finish.
	\item Right click on the project name |calendar_ARMv8_GCC| in the Project Explorer and choose Properties.
	\item Select C/C++ Build → Tool Chain Editor
	\item Uncheck the Display compatible toolchains only option.
	\item Select |GCC 6.1.1 [aarch64-elf]| as Current toolchain.
	\item Click Yes and OK.
	\item Right click on the project name in the Project Explorer and choose Build Project.
	\item Right click on the project name in the Project Explorer and choose Debug As, Debug Configurations...
	\item Open the option DS-5 Debugger, select |calendar_ARMv8_GCC-FM|, see \cref{fig:debugConfig} and click Debug and Yes.
	\item You can now simulate the application. You can see the output and enter the input in the Target Console window, see \cref{fig:targetConsole}. 
\end{itemize}

Please note: This example deliberately contains an error so you can practice your debug skills, see the file |readme.html| which is included in the project directory.

\figuur{scale=.75}{import}{The Import Projects dialog.}

\figuur{scale=.75}{debugConfig}{The Debug Configurations dialog.}

\figuur{scale=.75}{targetConsole}{The Target Console window.}

\chapter{Assignments}
You have installed ARM DS-5 and the Linaro toolchain and simulator now. 
In this chapter you will use these tools to develop and test a few LEGv8 assembler programs.
 
\section{Assignment 1. Testing a simple LEGv8 program}
This project consists of a very simple assembler function and a C program which calls the assembler function.

The C program \proglink{ass01/main.c} is given in \cref{lst:main}.

\floatlstinput[
style = cstyle, 
%style = linenumbers
]{ass01/main.c}{main}{A simple C program which calls our assembly function \proglink{ass01/main.c}.}

The |main| function calls the |test| function which is defined in the file \proglink{ass01/test.S} shown in \cref{lst:test}. The first argument is passed in register |X0| and the second argument is passed in register |X1|. The return value must be placed in register |X0|\footnote{%
This conforms with the Procedure Call Standard for the ARM 64-bit Architecture (AArch64), see \url{http://infocenter.arm.com/help/topic/com.arm.doc.ihi0055b/IHI0055B_aapcs64.pdf}.
}.

\floatlstinput{ass01/test.S}{test}{A very simple LEGv8 assembly function \proglink{ass01/test.S}.}

This very simple assembly program will return the sum of the two arguments. You can test this program as follows:
\begin{itemize}
	\item Visit \url{https://bitbucket.org/HarryBroeders/legv8} and Fork this repository.
	\item In DS-5, select File → Import... → Git → Projects from Git. Click Next.
	\item Select Clone URI and click Next.
	\item Fill in the URI to your own fork of the repository and click Next (twice).
	\item Fill in the Destination Directory and click Next.
	\item Select |ass01| and click Finish.
	\item Right click on the project name |ass01| in the Project Explorer and choose Build Project.
	\item Right click on the project name in the Project Explorer and choose Debug As, Debug Configurations...
	\item Open the option DS-5 Debugger, select |ass01| and click Debug and Yes.
\end{itemize}

When you run the program the output shown in \cref{fig:targetConsoleAss} should appear in the Target Console window.

\figuur{scale=.75}{targetConsoleAss}{The output of the \code{ass01} project.}

\section{Assignment 2. Writing a simple multiplication program}
\label{sec:mulOne}
Write an assembler routine for LEGv8 that can multiply two unsigned 32-bit integer values based on the C code shown in \cref{lst:mulOne}.

% #1 = (optional) extra key=value paren voor lstinputlisting b.v.
%      style = cstyle, cppstyle, mstyle of astyle (C, C++, MATLAB, AVR assembler)
%      style = linenumbers
%      linerange={1-6, 13-27}
% #2 = filenaam (moet in het directory ./progs staan)
% #3 = label=lst:#3
% #4 = titel
\floatlstinput[
	style = cstyle, 
	%style = linenumbers
]{mulOne.c}{mulOne}{A naive multiply algorithm \proglink{mulOne.c}.}

Please note: you are not really using a LEGv8 assembler but you are actually using an ARMv8 assembler. So please review the remarks made by Patterson and Hennessy \cite[Paragraph 2.23]{patterson2016}, which are summarized below:

\begin{itemize}
	\item Immediate versions are not separate assembly language instructions in ARMv8. While LEGv8 has |ADDI|, |SUBI|, etc, ARMv8 does not.
	You simply use the non-immediate version and use an immediate operand.
	For example, to use the LEGv8 instruction:
	|ADDI X0, X1, #4| you can just use |ADD  X0, X1, #4| (without the |I|).
	\item In the ARMv8 instruction set, register |X31| is |XZR| in most instructions but the stack pointer |SP| in others.
	In LEGv8 |X28| is used as stack pointer. 
	So you best use the symbolic names |XZR| and |SP| in your assembly code, to avoid confusion.
	\item The immediate field for the logic instructions are not simple 12-bit constants in ARMv8. 
	ARMv8 uses an algorithm to encode these immediate values. 
	This means that some values are valid and others are not (e.g., 0 and 5). 
\end{itemize}

Also write a C program which calls your assembler code to test it. You can assume that the value of parameter |a| is present in argument register |W0| and that parameter |b| is present in argument register |W1|. Also, the return value should be saved in result register |X0|.

Test your function by at least running:
\begin{itemize}
	\item |multiply(0, 0)|
	\item |multiply(0, 1)|
	\item |multiply(1, 0)|
	\item |multiply(1, 1)|
	\item |multiply(1, 4294967295)|
	\item |multiply(4294967295, 1)|
	\item |multiply(4294967295, 4294967295)|
\end{itemize}

If you like, you can use a test framework (e.g. \textmu{}nit, see: \url{https://nemequ.github.io/munit/}) to manage your testing.

How many instructions does it take your procedure to run the worst case \hcode[style=cstyle]{mul\\-tiply(4294967295, 4294967295)}?

%\section{Assignment 3. Saving some data on the stack}

\section{Assignment 3. A smarter multiply algorithm}
\label{sec:mulTwo}
As we have seen in \cref{sec:mulOne}, the multiply routine takes a lot of time in the worst case. There is a smarter method to multiply two values. The algorithm is shown in Figure 3.4 of the book \cite[page 194]{patterson2016}. It is actually explained to be for hardware, but it works in software as well. It is given in C for your convenience in \cref{lst:mulTwo}.

\floatlstinput[
style = cstyle, 
%style = linenumbers
]{mulTwo.c}{mulTwo}{A smarter multiply algorithm \proglink{mulTwo.c}.}

Adjust your multiply function of \cref{sec:mulOne} to work like the code given in \cref{lst:mulTwo} and test all the cases listed in \cref{sec:mulOne} again.

How many instructions does it take your procedure to run the worst case \hcode[style=cstyle]{mul\\-tiply(4294967295, 4294967295)}?

\section{Assignment 4. Using your multiply function to calculate powers}
\label{sec:power}
Now write an assembly function called |power| to calculate $n^m$ were $n$ and $m$ are both 32-bit unsigned integers. The result should be a 64-bit You have to call your own |multiply| function from your |power| function. Make sure your code is properly tested.
  

\end{clstShortInline}