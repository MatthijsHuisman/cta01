/**
 * Multiplies two unsigned int values
 * @param unsigned int a 			
	The value of a (0-4294967295) multiplicand
 * @param unsigned int b
	The value of b (0-4294967295) multiplier
 * @return unsigned long long int
	The answer of a times b
 */

unsigned long long int multiply(unsigned int a, unsigned int b)
{
	unsigned int i = 1;
	unsigned long long int answer = 0;

	for (i = 0; i < 64 ; i++)
	{
		if (b & 1 == 1) {
			answer = answer + a;
		}
		a = a << 1;
		b = b >> 1;
	}
	
	return answer;
}