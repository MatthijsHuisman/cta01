# Computer Architecture #

In dit repository is het Lab Work Handbook opgenomen dat bij de module CTA01 (Computer Architecture) van de minor Embedded Systems van de Hogeschool Rotterdam gebruikt worden.

Je kunt de gecompileerde versies (in pdf) vinden in de [Downloads](https://bitbucket.org/HR_ELEKTRO/cta01/downloads) van dit repository.

Op- en aanmerkingen zijn altijd welkom. Maak een [issue](https://bitbucket.org/HR_ELEKTRO/cta02/issues?status=new&status=open) aan of stuur een mail naar [Harry Broeders](mailto:j.z.m.broeders@hr.nl).